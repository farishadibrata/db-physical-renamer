const CREDS = {
  host: "localhost",
  user: "admin",
  password: "admin",
};
const CREDS_DEST = {
  host: "localhost",
  user: "admin",
  password: "admin",
}
const knex = require("knex")({
  client: "mssql",
  connection: {
    host: CREDS.host,
    user: CREDS.user,
    password: CREDS.password,
    ssl: false,
  },
});

const file = require("./generate_script");
const RESTRICTED_DATABASES = ["master", "tempdb", "model", "msdb"];
const SYNTAX_CHANGE = "_old";


let REQUIRED_FILES = [];
// Get List Databases
(async function renameLoop() {
  knex.raw("SELECT name FROM sys.databases").then((databases) => {
    for (let database of databases) {
      if (RESTRICTED_DATABASES.includes(database.name)) {
        continue;
      }
      let fileList = new file(`./scripts/fileList/${database.name}.json`);

      // Get Their required Files
      REQUIRED_FILES[database.name] = [];
      knex
        .raw(
          `
          SELECT d.name DatabaseName, f.physical_name AS PhysicalName FROM sys.master_files f 
          INNER JOIN sys.databases d ON d.database_id = f.database_id WHERE d.name = '${database.name}'
          `
        )
        .then(async (files) => {
          const bat = new file(`./scripts/rename/${database.name}.bat`, 1);
          const sql = new file(`./scripts/rename/${database.name}.sql`);
          // Deatach Database
          await bat.addText(
            `Sqlcmd -S ${CREDS.host} -d ${database.name} -U ${CREDS.user} -P ${CREDS.password} -Q "ALTER DATABASE ${database.name} SET SINGLE_USER;"`
          );
          await bat.addLine();
          await bat.addText(
            `Sqlcmd -S ${CREDS.host} -U ${CREDS.user} -P ${CREDS.password} -Q "EXEC sp_detach_db ${database.name};"`
          );
          await bat.addLine();
          const newFilenames = [];
          // rename Database file
          for (let file of files) {
            const splitted = file.PhysicalName.split(".");
            const fileName = file.PhysicalName.split("\\");
            REQUIRED_FILES[database.name].push(fileName[fileName.length - 1]);
            const newFilename =
              splitted[0] +
              "." +
              splitted[1] +
              SYNTAX_CHANGE +
              "." +
              splitted[2];
            newFilenames.push(newFilename);
            await bat.addLine();
            await bat.addText(`move "${file.PhysicalName}" "${newFilename}"`);
          }

          await sql.addText(`CREATE DATABASE ${database.name} ON`);
          await sql.addLine();
          for (let i = 0; i < newFilenames.length; i++) {
            await sql.addText(`(FILENAME = '${newFilenames[i]}')`);
            await sql.addLine();
            if (i !== newFilenames.length - 1) {
              await sql.addText(",");
            }
          }

          await sql.addText("FOR ATTACH;");
          await sql.addText(
            `ALTER DATABASE [${database.name}] MODIFY NAME = [${database.name + SYNTAX_CHANGE
            }];`
          );
          await sql.addText(
            `ALTER DATABASE ${database.name + SYNTAX_CHANGE} SET MULTI_USER;`
          );
          await bat.addLine();
          await bat.addText(
            `Sqlcmd -S ${CREDS.host}  -U ${CREDS.user} -P ${CREDS.password} -i "${database.name}.sql"`
          );
          await sql.save();
          await bat.save();
          await fileList.addText(JSON.stringify(REQUIRED_FILES[database.name]))
          await fileList.save()
        });
    }

  });
})();


const BACKUP_PATH = "C:\PURE_DB\DATA";
const DATA_PATH = "C:\\Program Files\\Microsoft SQL Server\\MSSQL10_50.MSSQLSERVER\\MSSQL\\DATA";
const NEW_DB_PREFIX = '_PURE';

const genCompare = function (creds_source, creds_target, database_a, database_b) {
  console.log(creds_source)
  return `
  Tools.NewDataComparison /SrcServerName ${creds_source.host} /SrcDatabaseName ${database_a}  /SrcUserName ${creds_source.user} /SrcPassword ${creds_source.password} /SrcDisplayName ${database_a} /TargetServerName ${creds_target.host} /TargetDatabaseName ${database_b} /TargeUserName ${creds_target.user} /TargetPassword ${creds_target.password} /TargetDisplayName ${database_b}
  `
};

  (async function importLoop() {
    const fs = require('fs');
    fs.readdirSync('./scripts/fileList').forEach(json => {
      const dbName = json.split('.')[0]
      const batchScript = new file(`./scripts/restore/${dbName}.bat`, 1);
      const sqlScript = new file(`./scripts/restore/${dbName}.sql`)
      fs.readFile(`./scripts/fileList/${json}`, 'utf8', function read(err, data) {
        if (err) {
          throw err;
        }
        const fileList = JSON.parse(data)
        for (let file of fileList) {
          batchScript.addText(`copy "${BACKUP_PATH}\\${file}" "${DATA_PATH}\\${file}"`)
          batchScript.addLine()
        }
        batchScript.addText(`Sqlcmd -S ${CREDS.host}  -U ${CREDS.user} -P ${CREDS.password} -i "${dbName}.sql" `)
        sqlScript.addText(`CREATE DATABASE ${dbName + NEW_DB_PREFIX} ON`);
        sqlScript.addLine();
        for (let i = 0; i < fileList.length; i++) {
          sqlScript.addText(`(FILENAME = '${DATA_PATH + '\\' + fileList[i]}')`);
          sqlScript.addLine();
          if (i !== fileList.length - 1) {
            sqlScript.addText(",");
          }
        }
        sqlScript.addText("FOR ATTACH;");
        batchScript.save()
        sqlScript.save()
        console.log(data)
      })
      const compareScript = new file(`./scripts/compare/${dbName}.txt`)
      compareScript.addText(genCompare(CREDS, CREDS_DEST, (dbName + SYNTAX_CHANGE), (dbName + NEW_DB_PREFIX)))
      compareScript.save()
    });
  })()