const fs = require("fs");

class file {
  constructor(filename, bash = 0) {
    this.filename = filename;
    this.data = "";
    if(bash === 1){
      this.initBash();
    }
  }

  async addLine() {
    this.data = this.data + "\n";
  }

  async addText(text) {
    this.data = this.data + text + " ";
  }

  async initBash() {
    this.addText("@echo off");
    this.addLine();
  }

  get compiled() {
    return this.data;
  }

  async save() {
    fs.writeFile(this.filename, this.data, function (err) {
      console.log('Error :',err)
      console.log("Script Generated");
    });
  }
}


module.exports = file